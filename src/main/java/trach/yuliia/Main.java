package trach.yuliia;

import java.util.Scanner;

/**
 * This is a main class of program.
 *
 * @author Yuliia Trach
 */
public class Main {
    /**
     * The entry point of program.
     *
     * @param args - array for output
     */
    public static void main(final String[] args) {
        System.out.println("You need to enter "
                + "the interval (for example - [1;100])");

        System.out.println("Please, enter the first number of interval: ");
        Scanner scanFirst = new Scanner(System.in, "UTF-8");
        int firstNumber = scanFirst.nextInt();

        System.out.println("And enter the last number of interval:");
        Scanner scanLast = new Scanner(System.in, "UTF-8");
        int lastNumber = scanLast.nextInt();

        /**
         * Creating new object.
         * @see OddAndEvenNumbers
         */
        OddAndEvenNumbers listNumbers = new OddAndEvenNumbers();
        listNumbers.printOddEvenNumbers(firstNumber, lastNumber);
        listNumbers.printSumOddEvenNumbers();

        System.out.println("\n\n"
                + "Please enter size of set numbers Fibonacci: ");
        Scanner scanSize = new Scanner(System.in, "UTF-8");
        int sizeSet = scanSize.nextInt();
        /**
         * Creating new object.
         * @see FibonacciNumbers
         */
        FibonacciNumbers numbers = new FibonacciNumbers();
        numbers.printFibonacciNumber(sizeSet);
        numbers.printBiggestNumbers();
        numbers.printPercentages();
    }
}

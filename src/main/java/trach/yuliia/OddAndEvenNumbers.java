package trach.yuliia;

import java.util.LinkedList;
import java.util.List;

/**
 * Class with odd and even numbers from interval.
 */
public class OddAndEvenNumbers {
    /**
     * Fields for odd numbers.
     */
    private List<Integer> oddNumbers = new LinkedList<Integer>();
    /**
     * fields for even numbers.
     */
    private List<Integer> evenNumbers = new LinkedList<Integer>();

    /**
     * Method for printing odd numbers.
     * from end to start and even numbers from start to end
     *
     * @param start - the first number of interval
     * @param end   - the last number of interval
     */
    public final void printOddEvenNumbers(final int start, final int end) {
        for (int i = end; i >= start; i--) {
            if (!(i % 2 == 0)) {
                oddNumbers.add(i);
            }
        }

        for (int j = start; j < end; j++) {
            if (j % 2 == 0) {
                evenNumbers.add(j);
            }
        }
        System.out.println("Odd numbers from start to the end of interval: "
                + oddNumbers + "\n"
                + "and even from end to start: " + evenNumbers);
    }

    /**
     * Method for calculating and.
     * printing the sum of even and odd numbers
     */
    public final  void printSumOddEvenNumbers() {
        int resultOddSum = 0;
        int resultEvenSum = 0;
        for (int k : oddNumbers) {
            resultOddSum += k;
        }
        for (int m : evenNumbers) {
            resultEvenSum += m;
        }
        System.out.println("The sum of odd numbers - " + resultOddSum
                + "  "
                + "and sum of even numbers - " + resultEvenSum);

    }
}

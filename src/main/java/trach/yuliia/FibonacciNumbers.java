package trach.yuliia;


import java.util.LinkedList;
import java.util.List;

/**
 * Class building Fibonacci numbers.
 */
public class FibonacciNumbers {
    /**
     * Field for fibonacciN numbers.
     */
    private List<Integer> fibonacciNumbers = new LinkedList<Integer>();
    /**
     * first variable of number.
     */
    private int a1 = 0;
    /**
     * Second variable for next number.
     */
    private int a2 = 1;
    /**
     * Third variable for calculation the nexr number.
     */
    private int a3 = 0;

    /**
     * Method for printing Fibonacci numbers.
     *
     * @param count - size of set numbers
     */
    public final void printFibonacciNumber(final int count) {
        fibonacciNumbers.add(a1);
        fibonacciNumbers.add(a2);

        for (int i = 2; i < count; i++) {
            a3 = a1 + a2;
            fibonacciNumbers.add(a3);
            a1 = a2;
            a2 = a3;
        }
        System.out.println("Fibonacci Numbers: " + fibonacciNumbers);
    }

    /**
     * Method for calculate and printing the biggest even and odd numbers.
     */
    public final void printBiggestNumbers() {
        int bigOdd = 0;
        int bigEven = 0;
        for (int j : fibonacciNumbers) {
            if (j % 2 == 0) {
                if (j > bigEven) {
                    bigEven = j;
                }
            } else {
                if (j > bigOdd) {
                    bigOdd = j;
                }
            }
        }
        System.out.println("The biggest even number is: F1 = " + bigEven
                + " and the biggest odd number is: F2 = " + bigOdd);
    }

    /**
     * Method for calculating and printing percentages of even and odd numbers.
     */
    public final void printPercentages() {
        int countOdd = 0;
        int countEven = 0;
        int oddPercent = 0;
        int evenPercent = 0;
        int hundredPercent = 100;
        for (int k : fibonacciNumbers) {
            if (k % 2 == 0) {
                countEven++;
            } else {
                countOdd++;
            }
        }
        oddPercent = (countOdd * hundredPercent)
                / fibonacciNumbers.size();
        evenPercent = (countEven * hundredPercent)
                / fibonacciNumbers.size();

        System.out.println("Odd %: " + oddPercent
                + " and Even %: " + evenPercent);
    }

}
